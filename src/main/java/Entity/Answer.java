package Entity;

import java.util.ArrayList;

public class Answer  {
    private String id;
    private String text;
    private ArrayList<Comment> comments;

    public Answer(String id, String text, ArrayList<Comment> comments) {
        this.id = id;
        this.text = text;
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", comments=" + comments +
                '}';
    }
}
