package Entity;

import java.util.ArrayList;

public class Profile {
    String name;
    ArrayList<Ask> asks;
    ArrayList<AskLink> askLinks;

    public Profile(String name, ArrayList<Ask> asks, ArrayList<AskLink> askLinks) {
        this.name = name;
        this.asks = asks;
        this.askLinks = askLinks;
    }

    public ArrayList<AskLink> getAskLinks() {
        return askLinks;
    }

    public void setAskLinks(ArrayList<AskLink> askLinks) {
        this.askLinks = askLinks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Ask> getAsks() {
        return asks;
    }

    public void setAsks(ArrayList<Ask> asks) {
        this.asks = asks;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "name='" + name + '\'' +
                ", asks=" + asks +
                ", askLinks=" + askLinks +
                '}';
    }
}
