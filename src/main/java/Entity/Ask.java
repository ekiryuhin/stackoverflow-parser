package Entity;

import java.util.ArrayList;

public class Ask  {
    private String URL;
    private String ask;
    private ArrayList<Answer> answers;

    public Ask(String URL, String ask, ArrayList<Answer> answers) {
        this.URL = URL;
        this.ask = ask;
        this.answers = answers;
    }


    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Ask{" +
                "ask='" + ask + '\'' +
                ", answers=" + answers +
                '}';
    }
}
