
import Entity.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class Parser {

    private String profileURL;
    private Document profile;

    public String getProfileURL() {
        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public Document getProfile() {
        return profile;
    }

    public void setProfile(Document profile) {
        this.profile = profile;
    }

    public Parser(String profileURL) throws IOException {
        this.profileURL = profileURL;
        profile = Jsoup.connect(profileURL).get();
    }

    public ArrayList<AskLink> getAskLinks() {

        ArrayList<AskLink> links = new ArrayList<>();
        Elements asks = profile.getElementsByAttributeValue("class", "question-hyperlink");
        asks.forEach(ask -> links.add(new AskLink(ask.text(), ask.attr("abs:href"))));
        return links;
    }

    public String getAskByLink(AskLink link) throws IOException {
        Document askPage = Jsoup.connect(link.getLink()).get();
        Element questionEl = askPage.getElementsByAttributeValue("class", "question").get(0);
        Element textQuestionEl = questionEl.getElementsByAttributeValue("class", "post-text").get(0);
        return textQuestionEl.text();
    }

    public ArrayList<Answer> getAnswersByLink(AskLink link) throws IOException {

        String answerId;
        ArrayList<Answer> answers = new ArrayList<>();
        Document askPage = Jsoup.connect(link.getLink()).get();

        Elements accAnswers = askPage.getElementsByAttributeValue("class", "answer accepted-answer");
        if (!accAnswers.isEmpty()) {
            for (Element accAnswer : accAnswers) {
                Elements texts = accAnswer.getElementsByAttributeValue("class", "post-text");
                for (Element text : texts) {
                    answerId = accAnswer.attr("id");
                    answers.add(new RightAnswer(answerId, text.text(), getCommentsByAnswer(answerId, link.getLink())));
                }
            }
        }

        Elements othAnswers = askPage.getElementsByAttributeValue("class", "answer");
        if (!accAnswers.isEmpty()) {
            for (Element othAnswer : othAnswers) {
                Elements texts = othAnswer.getElementsByAttributeValue("class", "post-text");
                for (Element text : texts) {
                    answerId = othAnswer.attr("id");
                    answers.add(new Answer(text.attr("id"), text.text(), getCommentsByAnswer(answerId, link.getLink())));
                }
            }
        }

        return answers;
    }

    public ArrayList<Comment> getCommentsByAnswer(String idAnswer, String URL) throws IOException {
        ArrayList<Comment> comments = new ArrayList<>();
        Document askPage = Jsoup.connect(URL).get();

        Element answerElement = askPage.getElementById(idAnswer);
        Elements commElements = answerElement.getElementsByAttributeValue("class", "comment-body");

        for (Element comment : commElements) {
            if (!comment.getElementsByAttributeValue("class", "comment-user").isEmpty()) {
                comments.add(new Comment(comment.getElementsByAttributeValue("class", "comment-copy").get(0).text(),
                        comment.getElementsByAttributeValue("class", "comment-user").get(0).text()));
            } else if (!comment.getElementsByAttributeValue("class", "comment-user owner").isEmpty()) {
                comments.add(new Comment(comment.getElementsByAttributeValue("class", "comment-copy").get(0).text(),
                        comment.getElementsByAttributeValue("class", "comment-user owner").get(0).text()));
            }
        }
        return comments;
    }

    public ArrayList<Ask> getProfileAsks() throws IOException {
        ArrayList<Ask> asks = new ArrayList<>();
        ArrayList<AskLink> askLinks = getAskLinks();
        for (AskLink link : askLinks) {
            asks.add(new Ask(link.getLink(), getAskByLink(link), getAnswersByLink(link)));
        }
        return asks;
    }

    public Profile parseProfile() throws MalformedURLException {

        MalformedURLException invalidURL = new MalformedURLException();
        try {
            Document page = Jsoup.connect(profileURL).get();
            String username = page.getElementsByAttributeValue("class", "user-card-name").first().ownText();
            if (page.getElementsByAttributeValue("class", "question-hyperlink").isEmpty()) {
                Elements viewMoreBlocks = page.getElementsByAttributeValue("class", "view-more");
                Element allQuestionsLink = viewMoreBlocks.select("a:contains(questions)").first();
                String allQuestionsPageURL = allQuestionsLink.attr("abs:href");
                Document allQuestionsPage = Jsoup.connect(allQuestionsPageURL).get();
                if (allQuestionsPage.getElementsByAttributeValue("class", "question-hyperlink").isEmpty()) {
                    throw invalidURL;
                } else {
                    this.profileURL = allQuestionsPageURL;
                    this.profile = allQuestionsPage;
                    ArrayList<Ask> asks = getProfileAsks();
                    return new Profile(username, asks, getAskLinks());
                }
            } else {
                ArrayList<Ask> asks = getProfileAsks();
                return new Profile(username, asks, getAskLinks());
            }
        } catch (IOException e) {
            throw invalidURL;
        }

    }


    public static void write(Profile profile) {

        System.out.println("------------------" + profile.getName() + "------------------");
        System.out.println("Questions list:");

        for (AskLink link : profile.getAskLinks()) {
            System.out.println(" - " + link.getTitle());
            System.out.println(link.getLink());
        }

        System.out.print("\n\n\n");
        System.out.println("----------------------------------------------------------------");
        for (Ask ask : profile.getAsks()) {
            System.out.println("Question:");
            System.out.println(ask.getAsk());

            int answNum = 1;
            for (Answer answer : ask.getAnswers()) {
                System.out.println("Answer #" + answNum + ":");
                System.out.println(answer.getText());
                System.out.println("\tComments:");
                int commNum = 1;
                for (Comment comment : answer.getComments()) {
                    System.out.println("\t Comment #" + commNum + " by " + comment.getAuthor() +":");
                    System.out.println("\t " + comment.getText());
                    commNum++;
                }
                answNum++;
            }
            System.out.println("------------------------");
            System.out.print("\n\n");
        }

    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Invalid arguments number");
        } else try {
            Parser parser;
            try {
                parser = new Parser(args[0]);
                System.out.println(parser.getProfileURL());
                write(parser.parseProfile());
                System.out.println(parser.getProfileURL());
            } catch (MalformedURLException e) {
                System.out.println("Invalid URL");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid URL");
        }
    }

}
